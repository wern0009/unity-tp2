using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class declencheur : MonoBehaviour
{
    public Text boxCounterText;  // Reference to the UI text for the box counter
    public Transform respawnPoint;  // Where the box will respawn
    private int boxCount = 0;  // The current count of boxes

    private void OnTriggerEnter(Collider other)
    {
        // Check if the object that entered the trigger is a box (or any specific tag you want)
        if (other.CompareTag("Box"))  // Ensure the box has the "Box" tag
        {
            // Increment the box count
            boxCount++;

            // Update the UI text
            UpdateBoxCounter();

            // Respawn the box by moving it to the respawn point
            RespawnBox(other.gameObject);
        }
    }

    // Update the text to show the current count of boxes
    private void UpdateBoxCounter()
    {
        boxCounterText.text = "Boxes: " + boxCount;
    }

    // Respawn the box by setting its position to the respawn point
    private void RespawnBox(GameObject box)
    {
        box.transform.position = respawnPoint.position;
        box.transform.rotation = respawnPoint.rotation;  // Optional: reset rotation if needed
        box.GetComponent<Rigidbody>().velocity = Vector3.zero;  // Reset any velocity the box had
    }
}
