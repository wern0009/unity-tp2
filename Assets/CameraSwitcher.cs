using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // Import the UI namespace

public class CameraSwitcher : MonoBehaviour
{
    public Camera[] cameras;  // Array of cameras
    public Text CameraText;   // Reference to the Text UI element
    private int currentCameraIndex = 0;  // Index of the currently active camera

    // Start is called before the first frame update
    void Start()
    {
        // Ensure only the first camera is enabled initially
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].gameObject.SetActive(i == currentCameraIndex);
        }

        // Update the UI text to display the current camera
        UpdateCameraText();
    }

    // Update is called once per frame
    void Update()
    {
        // Switch to the previous camera when Q is pressed
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchCamera(-1);
        }
        
        // Switch to the next camera when E is pressed
        if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchCamera(1);
        }
    }

    // Method to switch between cameras
    void SwitchCamera(int direction)
    {
        // Disable the currently active camera
        cameras[currentCameraIndex].gameObject.SetActive(false);
        
        // Update the camera index (looping around if necessary)
        currentCameraIndex = (currentCameraIndex + direction + cameras.Length) % cameras.Length;
        
        // Enable the new camera
        cameras[currentCameraIndex].gameObject.SetActive(true);

        // Update the UI text
        UpdateCameraText();
    }

    // Method to update the camera display text
    void UpdateCameraText()
    {
        CameraText.text = $"Camera {currentCameraIndex + 1}/{cameras.Length}";
    }
}
